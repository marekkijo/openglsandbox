#version 400

uniform sampler2D tex01;

in vec2 out_uv;

layout(location = 0) out vec4 fragColor;

void main() {
    fragColor = vec4(texture(tex01, out_uv).rrr, 1.0);
}
