#version 400

uniform sampler2D tex01;

in vec2 out_uv;

layout(location = 0) out vec4 fragColor;

float
    softEdgeMin = 0.5,
    softEdgeMax = 0.525;

void main() {
    float distValue = texture(tex01, out_uv).r;
    float color = smoothstep(softEdgeMin, softEdgeMax, distValue);
    //float color = distValue > 0.5 ? 1.0 : 0.0;
    //float color = distValue;
    fragColor = vec4(color, color, color, 1.0);
}
