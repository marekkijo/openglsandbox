#version 400

layout(location = 0) in vec4 position;
layout(location = 2) in vec2 uv;

out vec2 out_uv;

void main() {
    out_uv = uv;
    gl_Position = position;
}
