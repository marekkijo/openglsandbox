#version 400

#define M_PI  3.14159265359
#define M_2PI 6.28318530718
#define MAXDIST 0.05

uniform sampler2D tex01;
in vec2 out_uv;
layout(location = 0) out vec4 fragColor;

float steps = 8;
float stepVal = .5 / steps;

void main() {
    float inOrOut = texture(tex01, out_uv).r;
    float out_color = .5;
    float angleStepCount = 8;
    float curValue;
    float realR;
    vec2 uv;

    if (inOrOut < .5) {
        for (float r = 1; r <= steps; r ++) {
            out_color -= stepVal;
            realR = (MAXDIST / steps) * r;
            float angleStep = M_2PI / angleStepCount;
            for (float alpha = 0; alpha < angleStepCount; alpha ++) {
                uv.x = realR * cos(alpha * angleStep) + out_uv.x;
                uv.y = realR * sin(alpha * angleStep) + out_uv.y;
                if (uv.x < 0.0 || uv.y > 1.0 || uv.x < 0.0 || uv.y > 1.0)
                    continue;
                curValue = texture(tex01, uv).r;
                if (curValue > .5) {
                    fragColor = vec4(out_color, out_color, out_color, 1.0);
                    return;
                }
            }
            angleStepCount *= 2;
        }
    } else {
        for (float r = 1; r <= steps; r ++) {
            out_color += stepVal;
            realR = (MAXDIST / steps) * r;
            float angleStep = M_2PI / angleStepCount;
            for (float alpha = 0; alpha < angleStepCount; alpha ++) {
                uv.x = realR * cos(alpha * angleStep) + out_uv.x;
                uv.y = realR * sin(alpha * angleStep) + out_uv.y;
                if (uv.x < 0.0 || uv.y > 1.0 || uv.x < 0.0 || uv.y > 1.0)
                    continue;
                curValue = texture(tex01, uv).r;
                if (curValue < .5) {
                    fragColor = vec4(out_color, out_color, out_color, 1.0);
                    return;
                }
            }
            angleStepCount *= 2;
        }
    }
    fragColor = vec4(out_color, out_color, out_color, 1.0);
}
