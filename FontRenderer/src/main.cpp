#ifdef _MSC_VER
# if defined(_DEBUG)
#  pragma comment(linker, "/SUBSYSTEM:CONSOLE")
# else
#  pragma comment(linker, "/SUBSYSTEM:WINDOWS")
# endif
# pragma comment(linker, "/ENTRY:mainCRTStartup")
#endif

#include <GLPlatform/glwindow.h>
#include <GLTools/glmisc.h>
#include <memory>
#include <string>
#include "renderer.h"

int main(int argc, char *argv[]) {
    auto window = glplatform::GLWindow::createInstance();
    auto renderer = std::make_shared<Renderer>();

    window->setRedrawInterval(gltools::fps2msec(30));
    window->attachRenderer(renderer);
    if (!window->createWindow())
        return 1;

    return window->exec();
}
