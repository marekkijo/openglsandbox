#ifndef GLFONTRENDERERSDF_H
#define GLFONTRENDERERSDF_H

#include <string>

class GLFontRendererSDF {
public:
    GLFontRendererSDF();
    ~GLFontRendererSDF();
    unsigned int getBaseFontSize() {
        return _baseFontSize;
    }
    void setBaseFontSize(unsigned int baseFontSize) {
        _baseFontSize = baseFontSize;
    }
    unsigned int getDestFontSize() {
        return _destFontSize;
    }
    void setDestFontSize(unsigned int destFontSize) {
        _destFontSize = destFontSize;
    }
    float getDistanceSize() {
        return _distanceSize;
    }
    void setDistanceSize(float distanceSize) {
        _distanceSize = distanceSize;
    }

    bool loadFonts(std::string fontName);

    static void setFontsLocation(std::string fontsLocation) {
        s_fontsLocation = fontsLocation;
    }
    static std::string getFontsLocation() {
        return s_fontsLocation;
    }

private:
    unsigned int _baseFontSize{4096};
    unsigned int _destFontSize{128};
    float _distanceSize{0.1f};

    static std::string s_fontsLocation;

    bool loadPngIfExist(std::string basicString);
};

#endif // GLFONTRENDERERSDF_H
