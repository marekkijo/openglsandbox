#include "glfontrenderersdf.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <mutex>
#include <array>
#include <thread>

static const std::string GLFONTRENDERERSDG_FONTEXT{".ttf"};
#ifdef _MSC_VER
static const std::string DIRECTORY_SEPARATOR{"\\"};
static const std::string WRONG_DIRECTORY_SEPARATOR{"/"};
#else
static const std::string DIRECTORY_SEPARATOR{"/"};
static const std::string WRONG_DIRECTORY_SEPARATOR{"\\"};
#endif

static const char GLFONTRENDERERSDG_CHARACTERS[] {
    ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')',
    '*', '+', ',', '-', '.', '/', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', ':', ';', '<', '=',
    '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
    'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
    'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[',
    '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e',
    'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
    'z', '{', '|', '}', '~'
};
static const unsigned int GLFONTRENDERERSDG_CHARMAPSIZE = static_cast<unsigned int>(std::ceil(std::sqrt(sizeof(GLFONTRENDERERSDG_CHARACTERS) - 1)));
static const unsigned int GLFONTRENDERERSDG_ADDITIONALTHREADCOUNT = 4;

std::string GLFontRendererSDF::s_fontsLocation{"." + DIRECTORY_SEPARATOR};

struct GLFontRendererSDFLoaderThreadData {
    std::mutex mutex;
    cv::Mat texture;
    std::array<bool, sizeof(GLFONTRENDERERSDG_CHARACTERS)> taken;
};

struct GLFontRendererSDFLoaderThreadInfo {
    std::string fname;
    unsigned int baseFontSize;
    unsigned int destFontSize;
    float distanceSize;
};

class GLFontRendererSDFLoaderThread {
public:
    GLFontRendererSDFLoaderThread(GLFontRendererSDFLoaderThreadData *shared, GLFontRendererSDFLoaderThreadInfo &info) :
        _shared(shared),
        _info(info) { }
    ~GLFontRendererSDFLoaderThread() {
        if (!_initialized)
            return;
        finish();
    }
    void operator()() {
        if (!_initialized && !init())
            return;
        float distanceSize = std::sqrt(_info.baseFontSize * _info.baseFontSize) * _info.distanceSize / 2.f;
        float sizeRatio = static_cast<float>(_info.destFontSize) / static_cast<float>(_info.baseFontSize);
        while (takeIndexToProcess()) {
            if (!renderCurrentFont())
                continue;
            FT_Bitmap bitmap = _ftFace->glyph->bitmap;
            int
                shiftx = (_info.baseFontSize - bitmap.width) / 2.f + .5f,
                shifty = (_info.baseFontSize - bitmap.rows) / 2.f + .5f;

            assert(shiftx >= 0 && shifty >= 0);

            cv::Mat data(_info.baseFontSize, _info.baseFontSize, CV_8U, cv::Scalar(0));
            for (int y = 0; y < bitmap.rows; y ++)
                memcpy(data.row(y + shifty).data + shiftx, bitmap.buffer + y * bitmap.pitch, bitmap.width);

            data = data > 128;

            cv::Mat dataWhite;
            cv::distanceTransform(data, dataWhite, cv::DIST_L2, cv::DIST_MASK_3);
            dataWhite = cv::min(dataWhite, distanceSize);
            cv::bitwise_not(data, data);
            cv::Mat dataBlack;
            cv::distanceTransform(data, dataBlack, cv::DIST_L2, cv::DIST_MASK_3);
            dataBlack = cv::min(dataBlack, distanceSize);

            cv::Mat norm;
            cv::normalize(dataWhite - dataBlack, norm, 0.0, 1.0, cv::NORM_MINMAX);

            cv::Mat subTexture;
            cv::resize(norm, subTexture, cv::Size(), sizeRatio, sizeRatio, cv::INTER_LANCZOS4);
            cv::Mat subTexture8U;
            subTexture.convertTo(subTexture8U, CV_8U, 255);

            storeInDestinationTexture(subTexture8U);
        }
        finish();
    }
private:
    GLFontRendererSDFLoaderThreadData *_shared;
    const GLFontRendererSDFLoaderThreadInfo &_info;

    bool _initialized{false};
    FT_Face _ftFace{nullptr};
    FT_Library _ftLibrary{nullptr};
    int _index{0};

    void storeInDestinationTexture(const cv::Mat &subTex) {
        int x, y;
        countXY(&x, &y);
        std::unique_lock<std::mutex> lck(_shared->mutex);

        cv::Mat subTextureDest = _shared->texture(cv::Rect(_info.destFontSize * x,
                                                           _info.destFontSize * y,
                                                           _info.destFontSize, _info.destFontSize));
        subTex.copyTo(subTextureDest);
    }
    bool renderCurrentFont() {
        FT_UInt charIndex = FT_Get_Char_Index(_ftFace,
                                              GLFONTRENDERERSDG_CHARACTERS[_index]);
        if (FT_Load_Glyph(_ftFace, charIndex, FT_LOAD_RENDER))
            return false;
        if (_ftFace->glyph->bitmap.rows > _info.baseFontSize ||
            _ftFace->glyph->bitmap.width > _info.baseFontSize)
            return false;
        return true;
    }
    bool takeIndexToProcess() {
        std::unique_lock<std::mutex> lck(_shared->mutex);
        for (int i = _index; i < _shared->taken.size(); i ++)
            if (!_shared->taken[i]) {
                _shared->taken[i] = true;
                _index = i;
                return true;
            }
        return false;
    }
    void countXY(int *x, int *y) {
        *x = _index % GLFONTRENDERERSDG_CHARMAPSIZE;
        *y = _index / GLFONTRENDERERSDG_CHARMAPSIZE;
    }
    bool init() {
        assert(!_ftLibrary && !_ftFace);
        if (FT_Init_FreeType(&_ftLibrary) != 0)
            return false;
        if (FT_New_Face(_ftLibrary, _info.fname.c_str(), 0, &_ftFace) != 0) {
            finish();
            return false;
        }
        if (FT_Set_Pixel_Sizes(_ftFace, _info.baseFontSize, 0) != 0) {
            finish();
            return false;
        }
        _initialized = true;
        return true;
    }
    void finish() {
        if (_ftFace)
            FT_Done_Face(_ftFace);
        if (_ftLibrary)
            FT_Done_FreeType(_ftLibrary);
        _ftFace = nullptr;
        _ftLibrary = nullptr;
        _initialized = false;
    }
};


GLFontRendererSDF::GLFontRendererSDF() { }

GLFontRendererSDF::~GLFontRendererSDF() { }

bool GLFontRendererSDF::loadFonts(std::string fontName) {
    std::string fname = s_fontsLocation + fontName + GLFONTRENDERERSDG_FONTEXT;
    std::string outFileName = s_fontsLocation + fontName + "_" + std::to_string(_baseFontSize) + "_" + std::to_string(_destFontSize) + "(" + std::to_string(_distanceSize) + ").png";
    if (loadPngIfExist(outFileName))
        return true;

    GLFontRendererSDFLoaderThreadData threadData;
    threadData.texture = cv::Mat(_destFontSize * GLFONTRENDERERSDG_CHARMAPSIZE,
                                 _destFontSize * GLFONTRENDERERSDG_CHARMAPSIZE,
                                 CV_8U, cv::Scalar(0));
    threadData.taken.fill(false);
    GLFontRendererSDFLoaderThreadInfo threadInfo{fname, _baseFontSize, _destFontSize, _distanceSize};

    if (GLFONTRENDERERSDG_ADDITIONALTHREADCOUNT > 0) {
        std::array<std::thread, GLFONTRENDERERSDG_ADDITIONALTHREADCOUNT> threads;
        for (int i = 0; i < GLFONTRENDERERSDG_ADDITIONALTHREADCOUNT; i ++)
            threads[i] = std::thread{GLFontRendererSDFLoaderThread(&threadData, threadInfo)};
        for (auto &thread : threads)
            thread.join();
    } else {
        GLFontRendererSDFLoaderThread loaderThreadObject(&threadData, threadInfo);
        loaderThreadObject();
    }
    cv::flip(threadData.texture, threadData.texture, 0);
    
    return true;
}

bool GLFontRendererSDF::loadPngIfExist(std::string basicString) {
    return false;
}