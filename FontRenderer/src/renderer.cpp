#include <GL/glew.h>

#include "renderer.h"
#include <GLPlatform/glcontext.h>
#include <GL/gl.h>
#include <GLTools/gldebug.h>
#include <GLTools/multiindexer.h>
#include <glm/glm.hpp>
#include <algorithm>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <chrono>
#include <iostream>

Renderer::Renderer() { }

Renderer::~Renderer() { }

bool Renderer::init() {
    if (_initialized)
        return true;

    if (!_context->makeCurrent())
        return false;

    if (!initOpenGLExtensions())
        return false;

    glEnable(GL_DITHER);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    
    CHECK_GL_ERROR();

    gltools::GLShaderProgram::setShadersLocation(".\\shaders\\");

    if (!initShaders())
        return false;

    if (!initVertices())
        return false;

    if (!initTextures())
        return false;

    _initialized = true;

    resize(_width, _height);

    return true;
}

void Renderer::clean() {
    if (!_initialized)
        return;

    if (!_context->makeCurrent())
        return;

    glBindTexture(GL_TEXTURE_2D, 0);
    glDeleteTextures(1, &_sampleTexID);

    glBindVertexArray(0);
    glDeleteVertexArrays(1, &_planeVaoID);

    _shader.destroyShaderProgram();

    _initialized = false;
}

void Renderer::resize(unsigned int width, unsigned int height) {
    _width = width; _height = height;

    if (!_initialized)
        return;
    
    glViewport(0, 0, static_cast<GLsizei>(_width), static_cast<GLsizei>(_height));
    CHECK_GL_ERROR();
}

void Renderer::redraw() {
    if (!_initialized)
        return;

    _context->makeCurrent();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    static GLint tex01_loc = glGetUniformLocation(_shader.getID(), "tex01");

    _shader.use();
    CHECK_GL_ERROR();

    glUniform1i(tex01_loc, 0);
    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_2D, _sampleTexID);
    CHECK_GL_ERROR();

    glBindVertexArray(_planeVaoID);
    CHECK_GL_ERROR();

    glDrawArrays(GL_TRIANGLES, 0, 6);
    CHECK_GL_ERROR();

    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    _context->swapBuffers();
}

bool Renderer::initOpenGLExtensions() {
    glewExperimental = GL_TRUE;
    GLenum error = glewInit();

    if (error != GLEW_OK)
        return false;
    
    glGetError(); // GLEW issue workaround
    return true;
}

bool Renderer::initShaders() {
    if (!_shader.loadShaderProgram("smoothFont"))
        return false;
    return true;
}

bool Renderer::initVertices() {
    float sx = 0.3f, sy = 0.6f;
    float ex = 0.4f, ey = 0.7f;
    GLfloat plane[] = {
        -1.0f, -1.0f,   sx, sy,
         1.0f, -1.0f,   ex, sy,
        -1.0f,  1.0f,   sx, ey,
         1.0f, -1.0f,   ex, sy,
         1.0f,  1.0f,   ex, ey,
        -1.0f,  1.0f,   sx, ey
    };
    glGenVertexArrays(1, &_planeVaoID);
    glBindVertexArray(_planeVaoID);
    CHECK_GL_ERROR();

    GLuint vertexID;
    glGenBuffers(1, &vertexID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(plane), plane, GL_STATIC_DRAW);
    CHECK_GL_ERROR();

    GLint position = glGetAttribLocation(_shader.getID(), "position");
    glEnableVertexAttribArray(position);
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 4 * 4, reinterpret_cast<const GLvoid *>(0));
    CHECK_GL_ERROR();

    GLint uv = glGetAttribLocation(_shader.getID(), "uv");
    glEnableVertexAttribArray(uv);
    glVertexAttribPointer(uv, 2, GL_FLOAT, GL_FALSE, 4 * 4, reinterpret_cast<const GLvoid *>(2 * 4));
    CHECK_GL_ERROR();

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // ???: if deleted before glBindVertexArray(0) than couse crash at glDrawArrays
    // driver issue or shouldn't delete before make enable default VAO or shouldn't
    // delete at all
    glDeleteBuffers(1, &vertexID);

    CHECK_GL_ERROR();

    return true;
}

bool Renderer::initTextures() {
    _fontRenderer.setFontsLocation(".\\fonts\\");
    _fontRenderer.setBaseFontSize(2048);
    _fontRenderer.setDestFontSize(128);
    _fontRenderer.setDistanceSize(0.1f);
    //std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    _fontRenderer.loadFonts("asalees");
    //std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    //auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    //std::cout << "duration: " << duration << std::endl;;

    /*
    static unsigned int baseSize = 4096;
    static unsigned int destSize = 64;
    static char fontFile[] = ".\\fonts\\asalees.ttf";
    static char characters[] = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
    unsigned int matSize = static_cast<unsigned int>(std::ceil(std::sqrt(sizeof(characters) - 1)));
    cv::Mat texture(destSize * matSize, destSize * matSize, CV_8U, cv::Scalar(0));

    static float sizeRatio = static_cast<float>(destSize) / static_cast<float>(baseSize);
    static float distDepth = baseSize / 32.f;

    for (unsigned int y = 0; y < matSize; y ++)
        for (unsigned int x = 0; x < matSize; x ++) {
            unsigned int charIndex = y * matSize + x;
            if (charIndex >= sizeof(characters) - 1)
                continue;
            FT_Face face;
            cv::Mat data(baseSize, baseSize, CV_8U, cv::Scalar(0));
            if (loadFontFace(fontFile, characters[charIndex], baseSize, &face)) {
                FT_Bitmap bitmap = face->glyph->bitmap;
                int
                    shiftx = (baseSize - bitmap.width) / 2.0f + 0.5f,
                    shifty = (baseSize - bitmap.rows) / 2.0f + 0.5f;

                assert(shiftx >= 0 && shifty >= 0);

                cv::Mat grey(baseSize, baseSize, CV_8U, cv::Scalar(0));
                for (int y = 0; y < bitmap.rows; y ++)
                    memcpy(grey.row(y + shifty).data + shiftx, bitmap.buffer + y * bitmap.pitch, bitmap.width);

                releaseFontFace(&face);

                data = grey > 128;
            } else {
                continue;
            }

            cv::Mat dataWhite;
            cv::distanceTransform(data, dataWhite, cv::DIST_L2, cv::DIST_MASK_3);
            dataWhite = cv::min(dataWhite, distDepth);
            cv::bitwise_not(data, data);
            cv::Mat dataBlack;
            cv::distanceTransform(data, dataBlack, cv::DIST_L2, cv::DIST_MASK_3);
            dataBlack = cv::min(dataBlack, distDepth);

            cv::Mat norm;
            cv::normalize(dataWhite - dataBlack, norm, 0.0, 1.0, cv::NORM_MINMAX);

            cv::Mat subTexture;
            cv::resize(norm, subTexture, cv::Size(), sizeRatio, sizeRatio, cv::INTER_LANCZOS4);
            cv::Mat subTexture8U;
            subTexture.convertTo(subTexture8U, CV_8U, 255);

            cv::Mat subTextureDest = texture(cv::Rect(destSize * x, destSize * y, destSize, destSize));
            subTexture8U.copyTo(subTextureDest);
        }
    cv::imshow("texture", texture);
    cv::waitKey(0);
    cv::flip(texture, texture, 0);

    createTexture(texture);*/
    return true;
}

bool Renderer::loadFontFace(std::string fontName, char character, unsigned int fontSize, FT_Face *face) {
    static FT_Library library;
    if (!library &&
        FT_Init_FreeType(&library))
        return false;

    if (FT_New_Face(library, fontName.c_str(), 0, face))
        return false;

    if (FT_Set_Pixel_Sizes(*face, fontSize, 0))
        return false;

    FT_UInt charIndex = FT_Get_Char_Index(*face, character);

    if (FT_Load_Glyph(*face, charIndex, FT_LOAD_DEFAULT))
        return false;

    if ((*face)->glyph->format != FT_GLYPH_FORMAT_BITMAP &&
        FT_Render_Glyph((*face)->glyph, FT_RENDER_MODE_NORMAL))
        return false;

    if ((*face)->glyph->bitmap.rows <= fontSize && (*face)->glyph->bitmap.width <= fontSize)
        return true;

    releaseFontFace(face);
    return false;
}

void Renderer::releaseFontFace(FT_Face* face) {
    FT_Done_Face(*face);
}

void Renderer::createTexture(const cv::Mat &texture) {
    GLint unpackAlignment = texture.step & 3 ? 1 : 4;
    GLint unpackRowLength = texture.step / texture.elemSize();
    GLsizei width = texture.cols;
    GLsizei height = texture.rows;
    const GLvoid *data = texture.ptr();
    GLenum format = texture.channels() == 1 ? GL_RED : GL_BGR;
    GLenum type = GL_UNSIGNED_BYTE;
    if (texture.depth() == CV_8S)
        type = GL_BYTE;
    else if (texture.depth() == CV_16U)
        type = GL_UNSIGNED_SHORT;
    else if (texture.depth() == CV_16S)
        type = GL_SHORT;
    else if (texture.depth() == CV_32S)
        type = GL_INT;
    else if (texture.depth() == CV_32F)
        type = GL_FLOAT;

    if (_sampleTexID != 0) {
        glDeleteTextures(1, &_sampleTexID);
        _sampleTexID = 0;
    }
    glGenTextures(1, &_sampleTexID);
    glBindTexture(GL_TEXTURE_2D, _sampleTexID);

    glPixelStorei(GL_UNPACK_ALIGNMENT, unpackAlignment);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, unpackRowLength);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, format, type, data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);
    CHECK_GL_ERROR();
}

unsigned int Renderer::roundTo(unsigned int value, unsigned int round) {
    return value + (value % round ? round - value % round : 0);
}
