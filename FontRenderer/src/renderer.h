#ifndef RENDERER_H
#define RENDERER_H

#include <GLPlatform/glrenderer.h>
#include <GLTools/glshaderprogram.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_BITMAP_H
#include <vector>
#include "glfontrenderersdf.h"

namespace cv{
    class Mat;
}

namespace gltools{
class MultiIndexer;
}

class Renderer : public glplatform::GLRenderer {
public:
    Renderer();
    ~Renderer();

    bool init() override;
    void clean() override;
    void resize(unsigned width, unsigned height) override;
    void redraw() override;

private:
    bool _initialized{false};
    unsigned int _width{0};
    unsigned int _height{0};

    gltools::GLShaderProgram _shader;
    GLFontRendererSDF _fontRenderer;
    GLuint _planeVaoID{0};
    GLuint _sampleTexID{0};

    bool initOpenGLExtensions();
    bool initShaders();
    bool initVertices();
    bool initTextures();

    bool loadFontFace(std::string fontName, char character, unsigned int fontSize, FT_Face *face);
    void releaseFontFace(FT_Face *face);
    void createTexture(const cv::Mat &texture);

    static unsigned int roundTo(unsigned int value, unsigned int round);
};

#endif // RENDERER_H
