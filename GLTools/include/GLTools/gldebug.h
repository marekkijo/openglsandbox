#ifndef GLDEBUG_H
#define GLDEBUG_H

#if defined(_DEBUG)
#include "GLTools/gltools_api.h"

# define CHECK_GL_ERROR() do { gltools::debug_CHECK_GL_ERROR(); } while(false)

namespace gltools {
GLTOOLS_API void debug_CHECK_GL_ERROR();
}

#else
# define CHECK_GL_ERROR()
#endif

#endif // GLDEBUG_H
