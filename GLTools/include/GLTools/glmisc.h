#ifndef GLMISC_H
#define GLMISC_H

namespace gltools {
inline int fps2msec(int fps) { return static_cast<int>((1000. / fps) + .5); }
}

#endif // GLMISC_H
