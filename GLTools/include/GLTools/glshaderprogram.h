#ifndef GLSHADERPROGRAM_H
#define GLSHADERPROGRAM_H

#include "GLTools/gltools_api.h"
#include <string>
#ifdef _MSC_VER
# define NOMINMAX
# include <windows.h>
#endif
#include <GL/gl.h>

namespace gltools {
class GLTOOLS_API GLShaderProgram {
public:
    GLShaderProgram();
    ~GLShaderProgram();

    GLuint getID() {
        return _id;
    }
    bool loadShaderProgram(std::string programName);
    bool destroyShaderProgram();
    void use();

    static void setShadersLocation(std::string shadersLocation) {
        s_shadersLocation = shadersLocation;
    }
    static std::string getShadersLocation() {
        return s_shadersLocation;
    }

private:
    GLuint _id;
    bool _loaded;

    static std::string s_shadersLocation;
    
    static GLuint loadAndCompileShaderFile(const std::string &fname, GLenum shaderType);
    static bool fileExistsAndNotEmpty(const std::string &fname);
    static std::string loadShaderFile(const std::string &fname);
    static bool checkShaderCompileStatus(GLuint shaderID);
    static bool checkProgramLinkStatus(GLuint programID);
};
}

#endif // SHADERPROGRAM_H
