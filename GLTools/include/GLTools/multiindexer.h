#ifndef MULTIINDEXER_H
#define MULTIINDEXER_H

#include "GLTools/gltools_api.h"
#include <cassert>

namespace gltools {
class GLTOOLS_API MultiIndexer {
public:
    explicit MultiIndexer(unsigned int x, unsigned int y = 1, unsigned int z = 1);
    ~MultiIndexer();
    inline unsigned int operator()(unsigned int x, unsigned int y = 0, unsigned int z = 0) const {
        assert(x + y * _x + z * _x * _y < _x * _y * _z);
        return
            x +
            y * _x +
            z * _x * _y;
    }
    inline unsigned int x() const { return _x; }
    inline unsigned int y() const { return _y; }
    inline unsigned int z() const { return _z; }
    inline unsigned int width() const { return _x; }
    inline unsigned int height() const { return _y; }
    inline unsigned int depth() const { return _z; }
    inline unsigned int size() const { return _x * _y * _z; }
    void reset(unsigned int x, unsigned int y = 1, unsigned int z = 1);

private:
    unsigned int _x, _y, _z;
};
}


// TODO: a proposal of one type instead of couple
//       maybe I should try variadic template but have to check type
//       with some static_assert, left for now
/*
#include <initializer_list>
#include <vector>

GLTOOLS_API_EXTERN template class GLTOOLS_API std::vector<unsigned int, std::allocator<char32_t>>;

namespace gltools {
class GLTOOLS_API MultiIndexer {
public:
    MultiIndexer(std::initializer_list<unsigned int> indices);
    ~MultiIndexer();
    unsigned int operator()(std::initializer_list<unsigned int> indices);

private:
    std::vector<unsigned int> _indices;
};
}*/

#endif // SHADERPROGRAM_H
