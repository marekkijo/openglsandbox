#ifndef GLTOOLS_API_H
#define GLTOOLS_API_H

#ifdef _MSC_VER
# ifdef GLTOOLS_EXPORT
#  define GLTOOLS_API __declspec(dllexport)
#  define GLTOOLS_API_EXTERN
# else
#  define GLTOOLS_API __declspec(dllimport)
#  define GLTOOLS_API_EXTERN extern
# endif
#endif

#endif // GLTOOLS_API_H
