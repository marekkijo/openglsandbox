#include "GLTools/gldebug.h"
#include <iostream>
#include <cassert>
#ifdef _MSC_VER
# define NOMINMAX
# include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

namespace gltools {
#if defined(_DEBUG)
void debug_CHECK_GL_ERROR() {
    GLenum debug_errorNum = glGetError();

    if (debug_errorNum == GL_NO_ERROR)
        return;

    const GLubyte *debug_errorString = gluErrorString(debug_errorNum);
    std::cerr << debug_errorString << std::endl;
    assert(false);
}
#endif
}
