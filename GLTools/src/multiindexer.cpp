#include "GLTools/multiindexer.h"

namespace gltools {
MultiIndexer::MultiIndexer(unsigned int x, unsigned int y /* = 1 */, unsigned int z /* = 1 */) {
    reset(x, y, z);
}

MultiIndexer::~MultiIndexer() { }

void MultiIndexer::reset(unsigned int x, unsigned int y /* = 1 */, unsigned int z /* = 1 */) {
    _x = x; _y = y; _z = z;
}
}

/*
MultiIndexer::MultiIndexer(std::initializer_list<unsigned int> indices) :
_indices(indices) { }

MultiIndexer::~MultiIndexer() { }

unsigned int MultiIndexer::operator()(std::initializer_list<unsigned int> indices) {
    unsigned int result = *indices.begin();
    return result;
}*/
