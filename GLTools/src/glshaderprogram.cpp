#include <GL/glew.h>
#include "GLTools/glshaderprogram.h"
#include "GLTools/gldebug.h"
#include <iostream>
#include <fstream>
#include <vector>

static const std::string GLTOOLS_GLSHADERPROGRAM_VSEXT{".vs"};
static const std::string GLTOOLS_GLSHADERPROGRAM_FSEXT{".fs"};
#ifdef _MSC_VER
static const std::string GLTOOLS_DIRECTORY_SEPARATOR{"\\"};
static const std::string GLTOOLS_WRONG_DIRECTORY_SEPARATOR{"/"};
#else
static const std::string GLTOOLS_DIRECTORY_SEPARATOR{"/"};
static const std::string GLTOOLS_WRONG_DIRECTORY_SEPARATOR{"\\"};
#endif

namespace gltools {
std::string GLShaderProgram::s_shadersLocation{"." + GLTOOLS_DIRECTORY_SEPARATOR};

GLShaderProgram::GLShaderProgram() :
    _id {0},
    _loaded{false} {
}

GLShaderProgram::~GLShaderProgram() {
    destroyShaderProgram();
}

bool GLShaderProgram::loadShaderProgram(std::string programName) {
    if (_loaded)
        return false;
    std::string vsfname = s_shadersLocation + programName + GLTOOLS_GLSHADERPROGRAM_VSEXT;
    std::string fsfname = s_shadersLocation + programName + GLTOOLS_GLSHADERPROGRAM_FSEXT;

    GLuint vsShaderID = loadAndCompileShaderFile(vsfname, GL_VERTEX_SHADER);
    if (vsShaderID == 0) {
        return false;
    }
    CHECK_GL_ERROR();
    GLuint fsShaderID = loadAndCompileShaderFile(fsfname, GL_FRAGMENT_SHADER);
    if (fsShaderID ==0) {
        glDeleteShader(vsShaderID);
        return false;
    }
    CHECK_GL_ERROR();


    _id = glCreateProgram();
    glAttachShader(_id, vsShaderID);
    glAttachShader(_id, fsShaderID);
    glLinkProgram(_id);
    if (!checkProgramLinkStatus(_id)) {
        glDeleteProgram(_id);
        glDeleteShader(vsShaderID);
        glDeleteShader(fsShaderID);
        _id = 0;
        return false;
    }
    glDetachShader(_id, vsShaderID);
    glDeleteShader(vsShaderID);
    glDetachShader(_id, fsShaderID);
    glDeleteShader(fsShaderID);
    CHECK_GL_ERROR();

    _loaded = true;
    return true;
}

bool GLShaderProgram::destroyShaderProgram() {
    if (!_loaded)
        return false;

    glDeleteProgram(_id);
    CHECK_GL_ERROR();
    _id = 0;

    _loaded = false;
    return true;
}

void GLShaderProgram::use() {
    glUseProgram(_id);
}

GLuint GLShaderProgram::loadAndCompileShaderFile(const std::string &fname, GLenum shaderType) {
    if (!fileExistsAndNotEmpty(fname))
        return 0;

    GLuint shaderID = glCreateShader(shaderType);
    if (shaderID == 0)
        return 0;

    std::string code = loadShaderFile(fname);
    const GLchar *codeData = code.data();
    GLint codeLength = static_cast<GLint>(code.length());
    glShaderSource(shaderID, 1, &codeData, &codeLength);
    glCompileShader(shaderID);
    if (!checkShaderCompileStatus(shaderID)) {
        glDeleteShader(shaderID);
        return 0;
    }
    return shaderID;
}

bool GLShaderProgram::fileExistsAndNotEmpty(const std::string &fname) {
    std::ifstream file(fname, std::ifstream::in);
    return file.good() && file.peek() != EOF;
}

std::string GLShaderProgram::loadShaderFile(const std::string &fname) {
    std::ifstream file(fname, std::ifstream::in);
    file >> std::noskipws;
    std::string code{std::istreambuf_iterator<char>{file}, std::istreambuf_iterator<char>{}};
    return code;
}

bool GLShaderProgram::checkShaderCompileStatus(GLuint shaderID) {
    GLint success = 0;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);
    if (success == GL_TRUE)
        return true;
    std::cerr << "GL_COMPILE_STATUS == GL_FALSE:" << std::endl;
    GLint logLength = 0;
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength == 0) {
        std::cerr << "Compilation shader failed but info log length is ZERO." << std::endl;
        return false;
    }
    GLint logReedLength = 0;
    std::vector<GLchar> infoLog(logLength);
    glGetShaderInfoLog(shaderID, logLength, &logReedLength, infoLog.data());
    std::cerr << infoLog.data() << std::endl;
    return false;
}

bool GLShaderProgram::checkProgramLinkStatus(GLuint programID) {
    GLint success = 0;
    glGetProgramiv(programID, GL_LINK_STATUS, &success);
    if (success == GL_TRUE)
        return true;
    std::cerr << "GL_LINK_STATUS == GL_FALSE:" << std::endl;
    GLint logLength = 0;
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength == 0) {
        std::cerr << "Linking program failed but info log length is ZERO." << std::endl;
        return false;
    }
    GLint logReedLength = 0;
    std::vector<GLchar> infoLog(logLength);
    glGetProgramInfoLog(programID, logLength, &logReedLength, infoLog.data());
    std::cerr << infoLog.data() << std::endl;
    return false;
}
}
