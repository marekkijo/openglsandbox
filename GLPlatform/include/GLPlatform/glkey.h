#ifndef GLKEY_H
#define GLKEY_H

#include "GLPlatform/glplatform_api.h"
#include <cstdint>
#include <type_traits>

namespace glplatform {
enum class GLPLATFORM_API KeyCode : char {
    None                 = 0,
    Space                = ' ',
    ExclamationPoint     = '!',
    DoubleQuotes         = '"',
    NumberSign           = '#',
    DollarSign           = '$',
    PercentSign          = '%',
    Ampersand            = '&',
    SingleQuote          = '\'',
    OpeningParenthesis   = '(',
    ClosingParenthesis   = ')',
    Asterisk             = '*',
    PlusSign             = '+',
    Comma                = ',',
    MinusSign            = '-',
    Period               = '.',
    Slash                = '/',
    No0                  = '0',
    No1                  = '1',
    No2                  = '2',
    No3                  = '3',
    No4                  = '4',
    No5                  = '5',
    No6                  = '6',
    No7                  = '7',
    No8                  = '8',
    No9                  = '9',
    Colon                = ':',
    Semicolon            = ';',
    LessThanSign         = '<',
    EqualSign            = '=',
    GreaterThanSign      = '>',
    QuestionMark         = '?',
    AtSymbol             = '@',
    AA                   = 'A',
    BB                   = 'B',
    CC                   = 'C',
    DD                   = 'D',
    EE                   = 'E',
    FF                   = 'F',
    GG                   = 'G',
    HH                   = 'H',
    II                   = 'I',
    JJ                   = 'J',
    KK                   = 'K',
    LL                   = 'L',
    MM                   = 'M',
    NN                   = 'N',
    OO                   = 'O',
    PP                   = 'P',
    QQ                   = 'Q',
    RR                   = 'R',
    SS                   = 'S',
    TT                   = 'T',
    UU                   = 'U',
    VV                   = 'V',
    WW                   = 'W',
    XX                   = 'X',
    YY                   = 'Y',
    ZZ                   = 'Z',
    OpeningBracket       = '[',
    Backslash            = '\\',
    ClosingBracket       = ']',
    Caret                = '^',
    Underscore           = '_',
    GraveAccent          = '`',
    A                    = 'a',
    B                    = 'b',
    C                    = 'c',
    D                    = 'd',
    E                    = 'e',
    F                    = 'f',
    G                    = 'g',
    H                    = 'h',
    I                    = 'i',
    J                    = 'j',
    K                    = 'k',
    L                    = 'l',
    M                    = 'm',
    N                    = 'n',
    O                    = 'o',
    P                    = 'p',
    Q                    = 'q',
    R                    = 'r',
    S                    = 's',
    T                    = 't',
    U                    = 'u',
    V                    = 'v',
    W                    = 'w',
    X                    = 'x',
    Y                    = 'y',
    Z                    = 'z',
    OpeningBrace         = '{',
    VerticalBar          = '|',
    ClosingBrace         = '}',
    Tilde                = '~'
};

enum class GLPLATFORM_API KeySpecial : char {
    None        = 0,
    Backspace,
    Tab,
    Enter,
    Escape,
    PageUp,
    PageDown,
    End,
    Home,
    ArrowLeft,
    ArrowUp,
    ArrowRight,
    ArrowDown,
    PrintScreen,
    Insert,
    Delete,
    F01,
    F02,
    F03,
    F04,
    F05,
    F06,
    F07,
    F08,
    F09,
    F10,
    F11,
    F12
};

enum class GLPLATFORM_API KeyModifier : uint16_t {
    None            = 0x0000,
    Shift           = 0x0001,
    Control         = 0x0002,
    Alt             = 0x0004,
    LeftShift       = 0x0010,
    LeftControl     = 0x0020,
    LeftAlt         = 0x0040,
    RightShift      = 0x0100,
    RightControl    = 0x0200,
    RightAlt        = 0x0400,
    LeftWin         = 0x1000,
    RightWin        = 0x2000,
    CapsLock        = 0x4000,
    NumLock         = 0x8000
};

using T = std::underlying_type_t<KeyModifier>;

inline KeyModifier operator|(KeyModifier lhs, KeyModifier rhs) {
    return static_cast<KeyModifier>(static_cast<T>(lhs) | static_cast<T>(rhs));
}

inline KeyModifier& operator|=(KeyModifier& lhs, KeyModifier rhs) {
    lhs = static_cast<KeyModifier>(static_cast<T>(lhs) | static_cast<T>(rhs));
    return lhs;
}

inline KeyModifier operator&(KeyModifier lhs, KeyModifier rhs) {
    return static_cast<KeyModifier>(static_cast<T>(lhs) & static_cast<T>(rhs));
}

inline KeyModifier& operator&=(KeyModifier& lhs, KeyModifier rhs) {
    lhs = static_cast<KeyModifier>(static_cast<T>(lhs) & static_cast<T>(rhs));
    return lhs;
}
inline KeyModifier operator!(KeyModifier lhs) {
    return static_cast<KeyModifier>(!static_cast<T>(lhs));
}
}

#endif // GLKEY_H

