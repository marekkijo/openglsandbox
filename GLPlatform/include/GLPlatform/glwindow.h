#ifndef GLWINDOW_H
#define GLWINDOW_H

#include "GLPlatform/glplatform_api.h"
#include "GLPlatform/glwindowinfo.h"
#include "GLPlatform/glcontextinfo.h"
#include <memory>

namespace glplatform {
class GLRenderer;
}

namespace glplatform {
class
    GLPLATFORM_API GLWindow {
public:
    GLWindow();
    virtual ~GLWindow();

    const GLWindowInfo &getWindowInfo() const {
        return _windowInfo;
    }

    virtual void setWindowInfo(const GLWindowInfo &windowInfo) = 0;

    const GLContextInfo &getContextInfo() const {
        return _contextInfo;
    }

    virtual void setContextInfo(const GLContextInfo &contextInfo) = 0;
    virtual void attachRenderer(std::shared_ptr<GLRenderer> renderer) = 0;
    virtual void dettachRenderer() = 0;
    virtual void setRedrawInterval(int msec) = 0;
    virtual bool createWindow() = 0;
    virtual int exec() = 0;
    static std::shared_ptr<GLWindow> createInstance();

protected:
    GLWindowInfo _windowInfo = GLWindowInfo{
        true, true, false, false, true, true,
        640, 480, 0, 0, "GLWindow"
    };
    GLContextInfo _contextInfo = GLContextInfo{
        true, 32, 0, 24, 8, 4, 1,
        4, 0,
        GLContextInfo::Profile::Core,
        GLContextInfo::SwapBufferType::Double
    };
};
}

#endif // GLWINDOW_H

