#ifndef GLRENDERER_H
#define GLRENDERER_H

#include "GLPlatform/glplatform_api.h"
#include "GLPlatform/glkey.h"
#include "glcontext.h"
#include <memory>

namespace glplatform {
class
    GLPLATFORM_API GLRenderer {
public:
    virtual ~GLRenderer();

    void setContext(std::unique_ptr<GLContext> context) {
        _context = std::move(context);
    }

    std::unique_ptr<GLContext> getContext() {
        return std::move(_context);
    }

    virtual bool init() = 0;
    virtual void clean() = 0;
    virtual void resize(unsigned int width, unsigned int height) = 0;
    virtual void redraw() = 0;
    virtual void keyDown(KeyCode keyCode, KeySpecial keySpecial, KeyModifier keyModifier, int repeatCount);
    virtual void keyUp(KeyCode keyCode, KeySpecial keySpecial, KeyModifier keyModifier);

protected:
    std::unique_ptr<GLContext> _context{nullptr};
};
}

#endif // GLRENDERER_H

