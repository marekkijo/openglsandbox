#ifndef GLCONTEXT_H
#define GLCONTEXT_H

#include "GLPlatform/glplatform_api.h"

namespace glplatform {
class
    GLPLATFORM_API GLContext {
public:
    virtual ~GLContext();

    virtual bool makeCurrent() = 0;
    virtual bool swapBuffers() = 0;
};
}

#endif // GLCONTEXT_H

