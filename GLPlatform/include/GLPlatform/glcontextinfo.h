#ifndef GLCONTEXTINFO_H
#define GLCONTEXTINFO_H

#include "GLPlatform/glplatform_api.h"

namespace glplatform {
struct
    GLPLATFORM_API GLContextInfo {
    bool debugContext;
    unsigned int colorBufferSize;
    unsigned int accumBufferSize;
    unsigned int depthBufferSize;
    unsigned int stencilBufferSize;
    unsigned int samples;
    unsigned int swapInterval;
    unsigned int versionMajor;
    unsigned int versionMinor;

    enum class Profile {
        Core,
        Compatibility
    } profile;

    enum class SwapBufferType {
        Single,
        Double
    } swapBufferType;
};
}

#endif // GLCONTEXTINFO_H

