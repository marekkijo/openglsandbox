#ifndef GLPLATFORM_API_H
#define GLPLATFORM_API_H

#ifdef _MSC_VER
# ifdef GLPLATFORM_EXPORT
#  define GLPLATFORM_API __declspec(dllexport)
#  define GLPLATFORM_API_EXTERN
# else
#  define GLPLATFORM_API __declspec(dllimport)
#  define GLPLATFORM_API_EXTERN extern
# endif
#endif

#endif // GLPLATFORM_API_H

