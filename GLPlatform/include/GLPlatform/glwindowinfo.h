#ifndef GLWINDOWINFO_H
#define GLWINDOWINFO_H

#include "GLPlatform/glplatform_api.h"
#include <string>

namespace glplatform {
struct
    GLPLATFORM_API GLWindowInfo {
    bool hasBorder;
    bool hasCursor;
    bool autoCursorCenter;
    bool fullscreen;
    bool exitOnEscape;
    bool positionCentering;
    unsigned int width;
    unsigned int height;
    unsigned int posX;
    unsigned int posY;
    std::string name;
};
}

#endif // GLWINDOWINFO_H

