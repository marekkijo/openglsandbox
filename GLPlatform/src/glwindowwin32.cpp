#include "glwindowwin32.h"
#include "glcontexthandlewin32.h"
#include "glcontextwin32.h"
#include "GLPlatform/glrenderer.h"
#include "GLPlatform/glkey.h"
#include <algorithm>
#include <GL/gl.h>
#include <GL/wglext.h>

namespace glplatform {
struct KeyInfoBitField {
    unsigned int repeatCount : 16;
    unsigned int scanCode : 8;
    bool extendedKey : 1;
    unsigned int reserved : 4;
    bool contextCode : 1;
    bool previousKeyState : 1;
    bool transitionState : 1;
};

static PFNWGLGETPIXELFORMATATTRIBIVARBPROC wglGetPixelFormatAttribivARB{nullptr};
static PFNWGLGETPIXELFORMATATTRIBFVARBPROC wglGetPixelFormatAttribfvARB{nullptr};
static PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB{nullptr};
static PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB{nullptr};

const std::string GLWindowWin32::s_glWindowClassName{"glplatform__GLWindowWin32"};
const unsigned int GLWindowWin32::s_redrawTimerID{0};
unsigned int GLWindowWin32::s_glWindowClassInstanceCounter{0};

GLWindowWin32::GLWindowWin32() :
    _contextHandle{std::make_shared<GLContextHandleWin32>(nullptr, nullptr, nullptr)},
    _context{std::make_unique<GLContextWin32>(_contextHandle)} {
    GLWindowWin32::createWindowClass();
}

GLWindowWin32::~GLWindowWin32() {
    if(_renderer)
        _renderer->clean();
    _contextHandle->destroy();
    _contextHandle.reset();
    GLWindowWin32::destroyWindowClass();
}

void GLWindowWin32::setWindowInfo(const GLWindowInfo &windowInfo) {
    _windowInfo.hasBorder = windowInfo.hasBorder;
    _windowInfo.hasCursor = windowInfo.hasCursor;
    _windowInfo.autoCursorCenter = windowInfo.autoCursorCenter;
    _windowInfo.fullscreen = windowInfo.fullscreen;
    _windowInfo.exitOnEscape = windowInfo.exitOnEscape;
    _windowInfo.positionCentering = windowInfo.positionCentering;
    unsigned int res_width = ::GetSystemMetrics(SM_CXSCREEN);
    _windowInfo.width = std::min(windowInfo.width, res_width);
    unsigned int res_height = ::GetSystemMetrics(SM_CYSCREEN);
    _windowInfo.height = std::min(windowInfo.height, res_height);
    _windowInfo.posX = windowInfo.posX;
    _windowInfo.posY = windowInfo.posY;
    _windowInfo.name = windowInfo.name;
}

void GLWindowWin32::setContextInfo(const GLContextInfo &contextInfo) {
    _contextInfo.debugContext = contextInfo.debugContext;
    _contextInfo.colorBufferSize = std::min(contextInfo.colorBufferSize, 32U);
    _contextInfo.accumBufferSize = std::min(contextInfo.accumBufferSize, 32U);
    _contextInfo.depthBufferSize = std::min(contextInfo.depthBufferSize, 32U);
    _contextInfo.stencilBufferSize = std::min(contextInfo.stencilBufferSize, 32U);
    _contextInfo.samples = std::min(contextInfo.samples, 16U);
    _contextInfo.swapInterval = std::min(contextInfo.swapInterval, 1000U);
    _contextInfo.versionMajor = std::max(std::min(contextInfo.versionMajor, 4U), 1U);
    _contextInfo.versionMinor = std::min(contextInfo.versionMinor, 9U);
    _contextInfo.profile = contextInfo.profile;
    _contextInfo.swapBufferType = contextInfo.swapBufferType;
}

void GLWindowWin32::attachRenderer(std::shared_ptr<GLRenderer> renderer) {
    if (!_contextHandle->isValid()) {
        if (_renderer)
            dettachRenderer();
        _renderer = renderer;
        _renderer->setContext(std::move(_context));
    }
}

void GLWindowWin32::dettachRenderer() {
    if (!_contextHandle->isValid() && _renderer) {
        _context = _renderer->getContext();
        _renderer.reset();
    }
}

void GLWindowWin32::setRedrawInterval(int msec) {
    _redrawInterval = std::max(msec, -1);
    if (_contextHandle->isValid()) {
        if (_redrawInterval != -1)
            ::SetTimer(_contextHandle->getHWND(), s_redrawTimerID, _redrawInterval, nullptr);
        else
            ::KillTimer(_contextHandle->getHWND(), s_redrawTimerID);
    }
}

bool GLWindowWin32::createWindow() {
    if (!_renderer)
        return false;

    if (_contextHandle->isValid())
        return true;

    HWND hWnd = performWindowCreation();

    if (!hWnd)
        return false;

    HDC hDC;
    HGLRC hGLRC;
    if (!performContextCreation(hWnd, &hDC, &hGLRC)) {
        ::DestroyWindow(hWnd);
        return false;
    }

    _contextHandle->setHWND(hWnd);
    _contextHandle->setHDC(hDC);
    _contextHandle->setHGLRC(hGLRC);

    if (!_renderer->init()) {
        _contextHandle->destroy();
        return false;
    }

    if (_redrawInterval != -1)
        SetTimer(_contextHandle->getHWND(), s_redrawTimerID, _redrawInterval, nullptr);

    return true;
}

int GLWindowWin32::exec() {
    if (!_contextHandle->isValid())
        return 1;

    MSG msg = {0};

    while (::GetMessage(&msg, _contextHandle->getHWND(), 0, 0) != 0)
        ::DispatchMessage(&msg);

    return msg.message == WM_QUIT ? msg.wParam : 1;
}

void GLWindowWin32::parseKeyCodes(WPARAM wParam, LPARAM lParam, KeyCode *keyCode, KeySpecial *keySpecial, int *repeatCount) {
    KeyInfoBitField keyInfoBitField = reinterpret_cast<KeyInfoBitField &>(lParam);
    bool shiftOn = (_keyModifier & (KeyModifier::Shift | KeyModifier::LeftShift | KeyModifier::RightShift)) != KeyModifier::None;
    if (wParam >= 0x41 && wParam <= 0x5A) {
        int kc = wParam - 0x41;
        *keyCode = static_cast<KeyCode>(kc + static_cast<std::underlying_type_t<KeyCode>>(shiftOn ? KeyCode::AA : KeyCode::A));
    } else if (wParam >= 0x30 && wParam <= 0x39) {
        int kc = wParam - 0x30;
        *keyCode = static_cast<KeyCode>(kc + static_cast<std::underlying_type_t<KeyCode>>(KeyCode::No0));
        if (shiftOn) {
            switch (*keyCode) {
            case KeyCode::No0: *keyCode = KeyCode::ClosingParenthesis; break;
            case KeyCode::No1: *keyCode = KeyCode::ExclamationPoint;   break;
            case KeyCode::No2: *keyCode = KeyCode::AtSymbol;           break;
            case KeyCode::No3: *keyCode = KeyCode::NumberSign;         break;
            case KeyCode::No4: *keyCode = KeyCode::DollarSign;         break;
            case KeyCode::No5: *keyCode = KeyCode::PercentSign;        break;
            case KeyCode::No6: *keyCode = KeyCode::Caret;              break;
            case KeyCode::No7: *keyCode = KeyCode::Ampersand;          break;
            case KeyCode::No8: *keyCode = KeyCode::Asterisk;           break;
            case KeyCode::No9: *keyCode = KeyCode::OpeningParenthesis; break;
            default: break;
            }
        }
    } else {
        switch (wParam) {
        case VK_OEM_3:      *keyCode = shiftOn ? KeyCode::Tilde           : KeyCode::GraveAccent;
        case VK_OEM_MINUS:  *keyCode = shiftOn ? KeyCode::Underscore      : KeyCode::MinusSign;
        case VK_OEM_PLUS:   *keyCode = shiftOn ? KeyCode::PlusSign        : KeyCode::EqualSign;
        case VK_OEM_4:      *keyCode = shiftOn ? KeyCode::OpeningBrace    : KeyCode::OpeningBracket;
        case VK_OEM_6:      *keyCode = shiftOn ? KeyCode::ClosingBrace    : KeyCode::ClosingBracket;
        case VK_OEM_5:      *keyCode = shiftOn ? KeyCode::VerticalBar     : KeyCode::Backslash;
        case VK_OEM_1:      *keyCode = shiftOn ? KeyCode::Colon           : KeyCode::Semicolon;
        case VK_OEM_7:      *keyCode = shiftOn ? KeyCode::DoubleQuotes    : KeyCode::SingleQuote;
        case VK_OEM_COMMA:  *keyCode = shiftOn ? KeyCode::LessThanSign    : KeyCode::Comma;
        case VK_OEM_PERIOD: *keyCode = shiftOn ? KeyCode::GreaterThanSign : KeyCode::Period;
        case VK_OEM_2:      *keyCode = shiftOn ? KeyCode::QuestionMark    : KeyCode::Slash;
        default: break;
        }
    }
}

void GLWindowWin32::parseKeyModifiers(WPARAM wParam, LPARAM lParam) {
    KeyInfoBitField keyInfoBitField = reinterpret_cast<KeyInfoBitField &>(lParam);
    KeyModifier prev = _keyModifier;
    switch (wParam) {
    case VK_SHIFT:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::Shift;
        else
            _keyModifier |= KeyModifier::Shift;
        break;
    case VK_CONTROL:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::Control;
        else
            _keyModifier |= KeyModifier::Control;
        break;
    case VK_MENU:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::Alt;
        else
            _keyModifier |= KeyModifier::Alt;
        break;
    case VK_LSHIFT:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::LeftShift;
        else
            _keyModifier |= KeyModifier::LeftShift;
        break;
    case VK_LCONTROL:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::LeftControl;
        else
            _keyModifier |= KeyModifier::LeftControl;
        break;
    case VK_LMENU:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::LeftAlt;
        else
            _keyModifier |= KeyModifier::LeftAlt;
        break;
    case VK_RSHIFT:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::RightShift;
        else
            _keyModifier |= KeyModifier::RightShift;
        break;
    case VK_RCONTROL:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::RightControl;
        else
            _keyModifier |= KeyModifier::RightControl;
        break;
    case VK_RMENU:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::RightAlt;
        else
            _keyModifier |= KeyModifier::RightAlt;
        break;
    case VK_LWIN:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::LeftWin;
        else
            _keyModifier |= KeyModifier::LeftWin;
        break;
    case VK_RWIN:
        if (keyInfoBitField.previousKeyState)
            _keyModifier &= !KeyModifier::RightWin;
        else
            _keyModifier |= KeyModifier::RightWin;
        break;
    }
}

LRESULT GLWindowWin32::wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    switch (msg) {
    case WM_CREATE:
        return 0;
    case WM_CLOSE:
        _renderer->clean();
        _contextHandle->destroy();
        return 0;
    case WM_DESTROY:
        ::PostQuitMessage(0);
        return 0;
    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE && _windowInfo.exitOnEscape) {
            _renderer->clean();
            _contextHandle->destroy();
        } else {
            KeyCode keyCode = KeyCode::None;
            KeySpecial keySpecial = KeySpecial::None;
            int repeatCount = false;
            parseKeyModifiers(wParam, lParam);
            parseKeyCodes(wParam, lParam, &keyCode, &keySpecial, &repeatCount);
            if (keyCode != KeyCode::None || keySpecial != KeySpecial::None)
                _renderer->keyDown(keyCode, keySpecial, _keyModifier, repeatCount);
        }
        return 0;
    case WM_KEYUP: {
            KeyCode keyCode = KeyCode::None;
            KeySpecial keySpecial = KeySpecial::None;
            parseKeyModifiers(wParam, lParam);
            parseKeyCodes(wParam, lParam, &keyCode, &keySpecial);
            if (keyCode != KeyCode::None || keySpecial != KeySpecial::None)
                _renderer->keyUp(keyCode, keySpecial, _keyModifier);
        }
        return 0;
    case WM_TIMER:
        if (wParam == s_redrawTimerID) {
            _renderer->redraw();
            if (_redrawInterval != -1)
                SetTimer(hWnd, s_redrawTimerID, _redrawInterval, nullptr);
        }
        return 0;
    case WM_SIZE: {
        unsigned int width = LOWORD(lParam);
        unsigned int height = HIWORD(lParam);
        _renderer->resize(width, height);
        return 0;
    }
    default:
        break;
    }
    return ::DefWindowProc(hWnd, msg, wParam, lParam);
}

HWND GLWindowWin32::performWindowCreation() {
    DWORD dwStyle = WS_VISIBLE |
        (_windowInfo.hasBorder ? WS_OVERLAPPEDWINDOW : WS_POPUP);
    unsigned int
        posx = _windowInfo.posX,
        posy = _windowInfo.posY;
    if (_windowInfo.positionCentering)
        GLWindowWin32::calculateWindowCenterPosition(_windowInfo.width, _windowInfo.height, &posx, &posy);
    HINSTANCE hInstance = ::GetModuleHandle(nullptr);

    return ::CreateWindow(
        s_glWindowClassName.c_str(),    // _In_opt_ LPCTSTR   lpClassName
        _windowInfo.name.c_str(),       // _In_opt_ LPCTSTR   lpWindowName
        dwStyle,                        // _In_     DWORD     dwStyle
        posx, posy,                     // _In_     int       x, y
        _windowInfo.width,              // _In_     int       nWidth
        _windowInfo.height,             // _In_     int       nHeight
        nullptr,                        // _In_opt_ HWND      hWndParent
        nullptr,                        // _In_opt_ HMENU     hMenu
        hInstance,                      // _In_opt_ HINSTANCE hInstance
        static_cast<LPVOID>(this)       // _In_opt_ LPVOID    lpParam
    );
}

bool GLWindowWin32::performContextCreation(HWND hWnd, HDC *hDC, HGLRC *hGLRC) {
    HDC basicHDC;
    HGLRC basicHGLRC;

    if (!createBasicOpenGLContext(hWnd, &basicHDC, &basicHGLRC))
        return false;

    bool result = loadWGLExtensions();

    ::wglMakeCurrent(basicHDC, nullptr);
    ::wglDeleteContext(basicHGLRC);
    ::ReleaseDC(hWnd, basicHDC);

    if (!result)
        return false;

    return createMainOpenGLContext(hWnd, hDC, hGLRC);
}

bool GLWindowWin32::createBasicOpenGLContext(HWND hWnd, HDC *hDC, HGLRC *hGLRC) {
    *hDC = ::GetDC(hWnd);
    if (!*hDC)
        return false;

    DWORD dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
    dwFlags |= _contextInfo.swapBufferType == GLContextInfo::SwapBufferType::Double ? PFD_DOUBLEBUFFER : 0;
    
    PIXELFORMATDESCRIPTOR pfd{
        sizeof(PIXELFORMATDESCRIPTOR),  // WORD  nSize
        1,                              // WORD  nVersion
        dwFlags,                        // DWORD dwFlags
        PFD_TYPE_RGBA,                  // BYTE  iPixelType
        _contextInfo.colorBufferSize,   // BYTE  cColorBits
        0, 0,                           // BYTE  cRedBits, cRedShift
        0, 0,                           // BYTE  cGreenBits, cGreenShift
        0, 0,                           // BYTE  cBlueBits, cBlueShift
        0, 0,                           // BYTE  cAlphaBits, cAlphaShift
        _contextInfo.accumBufferSize,   // BYTE  cAccumBits
        0, 0, 0, 0,                     // BYTE  cAccumRedBits, cAccumGreenBits, cAccumBlueBits, cAccumAlphaBits
        _contextInfo.depthBufferSize,   // BYTE  cDepthBits
        _contextInfo.stencilBufferSize, // BYTE  cStencilBits
        0,                              // BYTE  cAuxBuffers
        PFD_MAIN_PLANE,                 // BYTE  iLayerType
        0,                              // BYTE  bReserved
        0, 0, 0                         // DWORD dwLayerMask, dwVisibleMask, dwDamageMask
    };

    int pfIndex = ::ChoosePixelFormat(*hDC, &pfd);
    if (!::SetPixelFormat(*hDC, pfIndex, &pfd) ||
        !(*hGLRC = ::wglCreateContext(*hDC))) {
        ::ReleaseDC(hWnd, *hDC);
        return false;
    }

    if (!wglMakeCurrent(*hDC, *hGLRC)) {
        ::wglMakeCurrent(*hDC, nullptr);
        ::wglDeleteContext(*hGLRC);
        ::ReleaseDC(hWnd, *hDC);
        return false;
    }

    return true;
}

bool GLWindowWin32::createMainOpenGLContext(HWND hWnd, HDC *hDC, HGLRC *hGLRC) {
    *hDC = ::GetDC(hWnd);
    if (!*hDC)
        return false;

    const unsigned int nMaxFormats = 1;
    int piFormats[nMaxFormats];
    unsigned int nNumFormats;
    int piAttribIList[] = {
        WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
        WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
        WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
        WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
        WGL_DOUBLE_BUFFER_ARB,
        _contextInfo.swapBufferType == GLContextInfo::SwapBufferType::Double ? GL_TRUE : GL_FALSE,
        WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
        WGL_COLOR_BITS_ARB, _contextInfo.colorBufferSize,
        WGL_ACCUM_BITS_ARB, _contextInfo.accumBufferSize,
        WGL_DEPTH_BITS_ARB, _contextInfo.depthBufferSize,
        WGL_STENCIL_BITS_ARB, _contextInfo.stencilBufferSize,
        GL_ZERO
    };

    PIXELFORMATDESCRIPTOR pfd;
    std::memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

    if (!wglChoosePixelFormatARB(*hDC, piAttribIList, nullptr, nMaxFormats, piFormats, &nNumFormats) ||
        !SetPixelFormat(*hDC, piFormats[0], &pfd)) {
        ::ReleaseDC(hWnd, *hDC);
        return false;
    }

    int attribList[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB, _contextInfo.versionMajor,
        WGL_CONTEXT_MINOR_VERSION_ARB, _contextInfo.versionMinor,
        WGL_CONTEXT_FLAGS_ARB,
        _contextInfo.debugContext ? WGL_CONTEXT_DEBUG_BIT_ARB : 0,
        WGL_CONTEXT_PROFILE_MASK_ARB,
        _contextInfo.profile == GLContextInfo::Profile::Core ?
            WGL_CONTEXT_CORE_PROFILE_BIT_ARB : WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
        GL_ZERO
    };

    *hGLRC = wglCreateContextAttribsARB(*hDC, nullptr, attribList);
    if (!*hGLRC) {
        ::ReleaseDC(hWnd, *hDC);
        return false;
    }

    if (!wglMakeCurrent(*hDC, *hGLRC)) {
        ::wglMakeCurrent(*hDC, nullptr);
        ::wglDeleteContext(*hGLRC);
        ::ReleaseDC(hWnd, *hDC);
        return false;
    }

    return true;
}

void GLWindowWin32::calculateWindowCenterPosition(unsigned int width, unsigned int height, unsigned int *posx, unsigned int *posy) {
    unsigned int
        resx = ::GetSystemMetrics(SM_CXSCREEN),
        resy = ::GetSystemMetrics(SM_CYSCREEN);
    *posx = static_cast<unsigned int>((resx - width) / 2.f + .5f);
    *posy = static_cast<unsigned int>((resy - height) / 2.f + .5f);
}

LRESULT GLWindowWin32::msgRouter(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    if (msg == WM_NCCREATE) {
        GLWindowWin32 *instance = reinterpret_cast<GLWindowWin32 *>(reinterpret_cast<LPCREATESTRUCT>(lParam)->lpCreateParams);
        if (!instance)
            return FALSE;
        ::SetWindowLongPtr(hWnd, GWL_USERDATA, reinterpret_cast<LONG_PTR>(instance));
        return TRUE;
    } else {
        GLWindowWin32 *instance = reinterpret_cast<GLWindowWin32 *>(::GetWindowLong(hWnd, GWL_USERDATA));
        if (instance)
            return instance->wndProc(hWnd, msg, wParam, lParam);
    }
    return ::DefWindowProc(hWnd, msg, wParam, lParam);
}

bool GLWindowWin32::loadWGLExtensions() {
    static bool wglExtensionsLoaded{false};
    if (wglExtensionsLoaded)
        return true;

    wglGetPixelFormatAttribivARB = reinterpret_cast<PFNWGLGETPIXELFORMATATTRIBIVARBPROC>(wglGetProcAddress("wglGetPixelFormatAttribivARB"));
    wglGetPixelFormatAttribfvARB = reinterpret_cast<PFNWGLGETPIXELFORMATATTRIBFVARBPROC>(wglGetProcAddress("wglGetPixelFormatAttribfvARB"));
    wglChoosePixelFormatARB = reinterpret_cast<PFNWGLCHOOSEPIXELFORMATARBPROC>(wglGetProcAddress("wglChoosePixelFormatARB"));
    wglCreateContextAttribsARB = reinterpret_cast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(wglGetProcAddress("wglCreateContextAttribsARB"));

    if (!wglGetPixelFormatAttribivARB || !wglGetPixelFormatAttribfvARB || !wglChoosePixelFormatARB || !wglCreateContextAttribsARB) {
        wglGetPixelFormatAttribivARB = nullptr;
        wglGetPixelFormatAttribfvARB = nullptr;
        wglChoosePixelFormatARB = nullptr;
        wglCreateContextAttribsARB = nullptr;
        return false;
    }

    wglExtensionsLoaded = true;
    return true;
}

void GLWindowWin32::createWindowClass() {
    if (s_glWindowClassInstanceCounter == 0) {
        HINSTANCE hInstance = ::GetModuleHandle(nullptr);
        HICON hIcon = ::LoadIcon(nullptr, IDI_WINLOGO);
        HCURSOR hCursor = ::LoadCursor(nullptr, IDC_ARROW);
        HBRUSH hBrush = reinterpret_cast<HBRUSH>(COLOR_BACKGROUND);

        WNDCLASS wndClass{
            CS_OWNDC | CS_DBLCLKS,      // UINT      style
            msgRouter,                  // WNDPROC   lpfnWndProc
            0,                          // int       cbClsExtra
            0,                          // int       cbWndExtra
            hInstance,                  // HINSTANCE hInstance
            hIcon,                      // HICON     hIcon
            hCursor,                    // HCURSOR   hCursor
            hBrush,                     // HBRUSH    hbrBackground
            nullptr,                    // LPCTSTR   lpszMenuName
            s_glWindowClassName.c_str() // LPCTSTR   lpszClassName
        };

        ::RegisterClass(&wndClass);
    }
    s_glWindowClassInstanceCounter++;
}

void GLWindowWin32::destroyWindowClass() {
    s_glWindowClassInstanceCounter--;
    if (s_glWindowClassInstanceCounter == 0) {
        HINSTANCE hInstance = ::GetModuleHandle(nullptr);
        ::UnregisterClass(
            s_glWindowClassName.c_str(),   // _In_     LPCTSTR   lpClassName
            hInstance                     // _In_opt_ HINSTANCE hInstance
        );
    }
}
}
