#include "glcontextwin32.h"
#include "glcontexthandlewin32.h"

namespace glplatform {
GLContextWin32::GLContextWin32(std::shared_ptr<GLContextHandleWin32> contextHandle) :
    _contextHandle(contextHandle) {
}

GLContextWin32::~GLContextWin32() {
}

bool GLContextWin32::makeCurrent() {
    if (!_contextHandle->isValid())
        return false;
    return ::wglMakeCurrent(_contextHandle->getHDC(), _contextHandle->getHGLRC()) == TRUE;
}

bool GLContextWin32::swapBuffers() {
    if (!_contextHandle->isValid())
        return false;
    return ::SwapBuffers(_contextHandle->getHDC()) == TRUE;
}
}
