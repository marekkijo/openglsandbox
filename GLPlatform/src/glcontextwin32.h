#ifndef GLCONTEXTWIN32_H
#define GLCONTEXTWIN32_H

#include "GLPlatform/glcontext.h"
#include <memory>

namespace glplatform {
class GLContextHandleWin32;

class GLContextWin32 final : public GLContext {
public:
    explicit GLContextWin32(std::shared_ptr<GLContextHandleWin32> contextHandle);
    ~GLContextWin32() override;
    bool makeCurrent() override;
    bool swapBuffers() override;

private:
    std::shared_ptr<GLContextHandleWin32> _contextHandle;
};
}

#endif // GLCONTEXTWIN32_H

