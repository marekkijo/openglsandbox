#ifndef GLCONTEXTHANDLEWIN32_H
#define GLCONTEXTHANDLEWIN32_H

#define NOMINMAX
#include <windows.h>

namespace glplatform {
class GLWindowWin32;

class GLContextHandleWin32 final {
    friend GLWindowWin32;
public:
    GLContextHandleWin32(HWND hWnd, HDC hDC, HGLRC hGLRC);
    ~GLContextHandleWin32();

    HWND getHWND() {
        return _hWnd;
    }

    HDC getHDC() {
        return _hDC;
    }

    HGLRC getHGLRC() {
        return _hGLRC;
    }

    bool isValid() {
        return !(!_hWnd || !_hDC || !_hGLRC);
    }

private:
    HWND _hWnd{nullptr};
    HDC _hDC{nullptr};
    HGLRC _hGLRC{nullptr};

    void setHWND(HWND hWnd) {
        _hWnd = hWnd;
    }

    void setHDC(HDC hDC) {
        _hDC = hDC;
    }

    void setHGLRC(HGLRC hGLRC) {
        _hGLRC = hGLRC;
    }

    void destroy();
};
}

#endif // GLCONTEXTHANDLEWIN32_H

