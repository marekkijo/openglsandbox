#include "GLPlatform/glwindow.h"
#include "glwindowwin32.h"

namespace glplatform {
GLWindow::GLWindow() {
}

GLWindow::~GLWindow() {
}

std::shared_ptr<GLWindow> GLWindow::createInstance() {
    return std::make_shared<GLWindowWin32>();
}
}
