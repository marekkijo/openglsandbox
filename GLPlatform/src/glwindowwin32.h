#ifndef GLWINDOWWIN32_H
#define GLWINDOWWIN32_H

#include "GLPlatform/glwindow.h"
#define NOMINMAX
#include <windows.h>
#include <GLPlatform/glkey.h>

namespace glplatform {
class GLContext;
class GLContextHandleWin32;

class GLWindowWin32 final : public GLWindow {
public:
    GLWindowWin32();
    ~GLWindowWin32();

    void setWindowInfo(const GLWindowInfo &windowInfo) override;
    void setContextInfo(const GLContextInfo &contextInfo) override;
    void attachRenderer(std::shared_ptr<GLRenderer> renderer) override;
    void dettachRenderer() override;
    void setRedrawInterval(int msec) override;
    bool createWindow() override;
    int exec() override;

private:
    std::shared_ptr<GLContextHandleWin32> _contextHandle{nullptr};
    std::unique_ptr<GLContext> _context{nullptr};
    std::shared_ptr<GLRenderer> _renderer;
    int _redrawInterval{-1};
    KeyModifier _keyModifier{KeyModifier::None};

    void parseKeyCodes(WPARAM wParam, LPARAM lParam, KeyCode *keyCode, KeySpecial *keySpecial, int *repeatCount = nullptr);
    void parseKeyModifiers(WPARAM wParam, LPARAM lParam);
    LRESULT wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    HWND performWindowCreation();
    bool performContextCreation(HWND hWnd, HDC *hDC, HGLRC *hGLRC);
    bool createBasicOpenGLContext(HWND hWnd, HDC *hDC, HGLRC *hGLRC);
    bool createMainOpenGLContext(HWND hWnd, HDC *hDC, HGLRC *hGLRC);

    static const std::string s_glWindowClassName;
    static const unsigned int s_redrawTimerID;
    static unsigned int s_glWindowClassInstanceCounter;

    static void calculateWindowCenterPosition(unsigned int width, unsigned int height, unsigned int *posx, unsigned int *posy);
    static LRESULT CALLBACK msgRouter(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
    static bool loadWGLExtensions();
    static void createWindowClass();
    static void destroyWindowClass();
};
}

#endif // GLWINDOWWIN32_H

