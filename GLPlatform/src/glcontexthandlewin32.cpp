#include "glcontexthandlewin32.h"

namespace glplatform {
GLContextHandleWin32::GLContextHandleWin32(HWND hWnd, HDC hDC, HGLRC hGLRC) :
    _hWnd{hWnd},
    _hDC{hDC},
    _hGLRC{hGLRC} {
}

GLContextHandleWin32::~GLContextHandleWin32() {
    destroy();
}

void GLContextHandleWin32::destroy() {
    if (_hGLRC && _hDC) {
        ::wglMakeCurrent(_hDC, nullptr);
        ::wglDeleteContext(_hGLRC);
    }
    if (_hDC && _hWnd) {
        ::ReleaseDC(_hWnd, _hDC);
    }
    if (_hWnd) {
        ::DestroyWindow(_hWnd);
    }
    _hWnd = nullptr;
    _hDC = nullptr;
    _hGLRC = nullptr;
}
}
